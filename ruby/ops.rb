# Operadores de comparação

#v1 = 10
#v2 = 20

# comparador em ruby, sempre retornam falso ou verdadeiro

# estamos obtendo o resulltado da comparação entre v1 e v2

#  == para saber se é igual
#  != para saber se é diferente
#  .eql?() para saber se é igual
#  > para saber se é maior
#  < para saber se é menor
# >= para saber se é maior ou igual
# .equals para saber se o objeto é igual
#  <= para saber se é menos ou igual

#res = v1 == v2

#puts res


# Operadores aritméticos
# Subtrair => -
# Soma => +
# Multiplicar => *
# Dividir => /

v1 = 10
v2 = 10

res = v1 + v2
puts res