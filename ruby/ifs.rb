

# IFs em Ruby funcionam com os Operadores de comparação
# Operadores de comparação em Ruby sempre retornam falso ou verdadeiro

v1 = 10
v2 = 20

if v1 == v2
  puts 'Sim são iguais'
else
  puts 'Não são iguais '
end

saldo = 1000

if saldo > 0
  puts 'Beleza, pode gastar'
else
  puts 'Deu ruim. Liga pro pai.'
end

if saldo > 500
  puts 'Partiu CIROC'
elsif saldo < 100
  puts 'Estamos bem'
else
  puts 'Aff'
end
