

  class Cachorro
    attr_accessor :nome, :cor, :idade, :raca

    # contrutor é um metodo que roda automaticamente
    def initialize(nome, idade, raca, cor)
     # já deve atribuir o nome ao cachorro
     @nome = nome
     @idade = idade
     @raca = raca
     @cor = cor
    end

    # metodo late
    def late
      puts @nome + ' Diz: au, au...'
    end

   def monstra_idade
     puts @idade + '@nome'
   end
  end



  spike = Cachorro.new('Spike', 5, 'Buldog', 'Pardo')
  spike.late
  spike.monstra_idade

  snoop = Cachorro.new('Snoop', 10, 'ViraLata', 'Carabelo')
  snoop.late

